# accordion

If you use Talgos BE-Theme inside LEPTON as standard BE-Theme, here are the neccessary files for BE-Theme Talgos. <br />
Please upload all files via ftp to <br />
installation-root/templates/talgos/backend/accordion<br />
and you are done.<br />
<br />
Please notice, that all files are published under the same license as LEPTON Core.<br />
For details please see license information delivered with LEPTON Package.<br />
